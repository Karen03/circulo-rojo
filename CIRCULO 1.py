import pygame 
pygame.init()

Ancho= 600
Alto = 600

Blanco= (255, 255, 255)
Negro= (0,0,0)
Rojo=(255,0,0)

screen= pygame.display.set_mode((Ancho,Alto))
clock=pygame.time.Clock()

pos_x=100
pos_y=100
running=True
while running:
  
    for event in pygame.event.get():
        if event.type==pygame.QUIT:
            running=False
        if event.type==pygame.KEYDOWN:
            if event.key==pygame.K_ESCAPE:
                running=False
            if event.key == pygame.K_RIGHT:
                pos_y+=10 
            if event.key == pygame.K_LEFT:
                pos_y-=10
            if event.key == pygame.K_DOWN:
                pos_y+=10
            if event.key == pygame.K_UP:
                pos_y-=10 
            
    screen.fill(Rojo)            

    pygame.draw.circle(screen,Blanco, ((pos_x,pos_y)), 30)

    pygame.display.update()
 
pygame.quit()